package com.alacarte.mstpbackend.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class Authenticator {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${user.authenticator.uri}")
    private String uri;

    public String authenticate(String apiKey) {
        String fullUri = uri + "/authenticate/" + apiKey;
        ResponseEntity<String> response = restTemplate.getForEntity(fullUri, String.class);
        return response.getBody();
    }

}
